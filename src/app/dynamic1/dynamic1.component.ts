import { ConfigService } from './../module1/config.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dynamic1',
  templateUrl: './dynamic1.component.html',
  styleUrls: ['./dynamic1.component.scss']
})
export class Dynamic1Component implements OnInit {

  constructor(private config: ConfigService) { }

  ngOnInit(): void {
    console.log(this.config._userName);
  }

}
