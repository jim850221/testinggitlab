import { Dynamic1Component } from './dynamic1/dynamic1.component';
import { Component, ComponentFactoryResolver, ViewChild } from '@angular/core';
import { DynamicDirective } from './dynamic.directive';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @ViewChild(DynamicDirective, {static: true}) dir: DynamicDirective;
  title = 'dynamicModule';

  constructor(private componentFactoryResolver: ComponentFactoryResolver) {}

  addComponent() {
    console.log(this.dir);
    const factory = this.componentFactoryResolver.resolveComponentFactory<any>(Dynamic1Component);
    this.dir.viewContainerRef.clear();
    this.dir.viewContainerRef.createComponent(factory);
  }
}
