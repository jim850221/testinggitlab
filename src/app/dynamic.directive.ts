import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[Dynamic]'
})
export class DynamicDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
