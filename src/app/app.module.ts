import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Module1Module } from './module1/module1.module';
import { DynamicDirective } from './dynamic.directive';

@NgModule({
  declarations: [
    AppComponent,
    DynamicDirective,
  ],
  imports: [
    Module1Module,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
